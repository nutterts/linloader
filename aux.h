#ifndef AUX_H
#define AUX_H

#include "rpi.h"
#include "obj.h"

/// REGISTERS
// Memory offset
#define AUX_BASE RPI_PBASE+0x215000

// Enable/disable device
#define AUX_ENABLES AUX_BASE+0x04

// Mini-uart registers
#define AUX_MU_IO_REG AUX_BASE+0x40
#define AUX_MU_IER_REG AUX_BASE+0x44
#define AUX_MU_IIR_REG AUX_BASE+0x48
#define AUX_MU_LCR_REG AUX_BASE+0x4C
#define AUX_MU_MCR_REG AUX_BASE+0x50
#define AUX_MU_LSR_REG AUX_BASE+0x54
#define AUX_MU_MSR_REG AUX_BASE+0x58
#define AUX_MU_SCRATCH AUX_BASE+0x5C
#define AUX_MU_CNTL_REG AUX_BASE+0x60
#define AUX_MU_STAT_REG AUX_BASE+0x64
#define AUX_MU_BAUD_REG AUX_BASE+0x68
#define AUX_MU_CLOCK 250000000

// SPI registers
#define AUX_SPI0_CNTL0_REG AUX_BASE+0x80
#define AUX_SPI0_CNTL1_REG AUX_BASE+0x84
#define AUX_SPI0_STAT_REG AUX_BASE+0x88
#define AUX_SPI0_IO_REG AUX_BASE+0x90
#define AUX_SPI0_PEEK_REG AUX_BASE+0x94

/// OBJECTS

// Mini-uart
typedef struct {
    obj My;
    voidint_func Init;
    charvoid_func Peek;
    voidchar_func Poke;
} muart_obj;
extern muart_obj muart;

// SPI
typedef struct {
    obj My;
    voidint_func Init;
    charvoid_func Peek;
    voidchar_func Poke;
} spi_obj;
extern spi_obj spi;


#endif // AUX_H
