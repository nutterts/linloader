#include "rpi.h"
#include "gpio.h"

extern char _binary_linhigh_bin_start;
extern char _binary_linhigh_bin_end;

void
kmain()
{
    gpio.Mode(GPLED_POWER, GPMODE_OUT);
    gpio.Mode(GPLED_ACKOK, GPMODE_OUT);

    /*
    *   This could be way more optimized but
    *   is failsafe and gets the job done.
    *
    *`  Copies loader to 0x5000000
    *   ==========================
    *
    *   Stack:  0x4FFFFFF - 0x4F00000
    *   Heap:   0x4000000 - 0x4EFFFFF
    */
    char  *p = &_binary_linhigh_bin_start;
    char  *d = (void *) 0x5000000;

    int gpiostate = 0;
    int c = 0;

    while (p != &_binary_linhigh_bin_end)
    {
        if ((c % 0x100000) == 0)
        {
            if (gpiostate)
            {
                gpiostate = 0;
                gpio.Set(GPLED_POWER);
                gpio.Clear(GPLED_ACKOK);
            }
            else
            {
                gpiostate = 1;
                gpio.Clear(GPLED_POWER);
                gpio.Set(GPLED_ACKOK);
            }
        }
        *d++ = *p++;
        c++;
    }
    gpio.Set(GPLED_POWER);
    gpio.Clear(GPLED_ACKOK);
}
