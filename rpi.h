#ifndef RPI_H
#define RPI_H

typedef unsigned int uint;

extern void poke (unsigned int, unsigned int);
extern uint peek (unsigned int);
extern void dummy ( unsigned int );
extern void hang ( void );

extern unsigned int RPI_PBASE;

/// FUNCTION POINTERS
typedef void (*voidint_func)(int);
typedef void (*voidvoid_func)(void);
typedef void (*voidchar_func)(char);
typedef char (*charvoid_func)(void);
typedef int (*intint_func)(int);


#endif // RPI_H
