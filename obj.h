#ifndef OBJ_H
#define OBJ_H

typedef enum {
    RPI_OBJ, GPIO_OBJ, MUART_OBJ, SPI_OBJ
} obj_id;

typedef struct {
    int _magic;
    obj_id Id;
} obj;

#define OBJ_MAGIC 0x0BD00D // 774157

#endif // OBJ_H
