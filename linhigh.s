.section ".text.boot"

.globl __entry
.extern kmain

__entry:
    mov r0, pc
    bl kmain
