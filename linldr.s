.section ".text"

.globl hang
.globl peek
.globl poke
.globl dummy

hang:
    b hang

peek:
    ldr r0, [r0]
    bx lr

poke:
    str r1, [r0]
    bx lr

dummy:
    subs r0, r0, #1
    bne dummy
    bx lr
