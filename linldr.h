#ifndef LINLDR_H
#define LINLDR_H

#define LINLDR_VER_HI 0
#define LINLDR_VER_LO 1

// Amount of memory reserved for bootloader.
static const int LINLDR_MEMLIMIT = 0x1000000; // 16MB

#endif // LINLDR_H
