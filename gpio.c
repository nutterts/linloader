#include "rpi.h"
#include "gpio.h"

/// LED PINS
int GPLED_ACKOK = 47;
int GPLED_POWER = 35;

/// UART PINS
int GPUART_TX = 14;
int GPUART_RX = 15;

/// SPI PINS
int GPSPI_CE1  = 7;
int GPSPI_CE0  = 8;
int GPSPI_MISO = 9;
int GPSPI_MOSI = 10;
int GPSPI_SCLK = 11;

/// GPIO PINS
void gpio_mode(int pin, gpmode mode)
{
    unsigned int reg = GPFSEL0 + ((pin/10) * 4);
    unsigned int bit = (pin%10) * 3;
    poke(reg,((peek(reg) & ~(7 << bit)) | mode << bit));
}

void gpio_set(int pin)
{
    poke(GPSET0 + ((pin/32) * 4), 1 << (pin%32));
}

void gpio_clear(int pin)
{
    poke(GPCLR0 + ((pin/32) * 4), 1 << (pin%32));
}

/// ASSERT CLK ON PIN
void gpio_clk(int pin)
{
    uint clkreg = GPPUDCLK0 + ((pin/32)*4);
    uint clkbit = (pin%32);

    poke(GPPUD, 0);
    dummy(150);
    poke(clkreg, 1 << clkbit);
    dummy(150);
    poke(GPPUD, 0);
 }

/// OBJECT
gpio_obj gpio = {
                    .My = {
                           ._magic = OBJ_MAGIC,
                           .Id = GPIO_OBJ,
                           },
                    .Mode = gpio_mode,
                    .Set = gpio_set,
                    .Clear = gpio_clear,
                    .AssertClk = gpio_clk,
                    };
