ARMGNU ?= arm-none-eabi

AOPS = --warn --fatal-warnings -mcpu=cortex-a7 -mfpu=neon-vfpv4

COPS = -Wall -O2 -nostartfiles -ffreestanding -mcpu=cortex-a7 -mfpu=neon-vfpv4

LIB = -L$(HOME)/arm-none-eabi/lib/ -L$(HOME)/lib/gcc/arm-none-eabi/5.2.0

SOURCES_LOW = linlow.o linldr.o rpi.c.o gpio.c.o linlow.c.o
TARGETS_LOW = linlow.elf linlow.bin linlow.list linlow.hex

SOURCES_HIGH = linhigh.o linldr.o rpi.c.o gpio.c.o aux.c.o newlib.c.o linldr.c.o
TARGETS_HIGH = linhigh.elf linhigh.bin linhigh.list linhigh.hex

all: clean linldr.bin linldr.list linlow.list
clean:
	rm -f *.o
	rm -f *.bin
	rm -f *.hex
	rm -f *.elf
	rm -f *.list
	rm -f *.obj

%.o: %.s
	$(ARMGNU)-as $(AOPS) -o $@ $<

%.c.o: %.c
	$(ARMGNU)-gcc $(COPS) -o $@ -c $<


linlow.elf: $(SOURCES_LOW) linhigh.obj
		$(ARMGNU)-ld $(SOURCES_LOW) linhigh.obj -T linlow.ld -o linlow.elf

linldr.bin: linlow.elf
		$(ARMGNU)-objcopy linlow.elf -O binary linldr.bin

linlow.list: linlow.elf
		$(ARMGNU)-objdump -D linlow.elf > linlow.list


linhigh.elf: $(SOURCES_HIGH)
		$(ARMGNU)-ld $(SOURCES_HIGH) -T linhigh.ld -o linhigh.elf $(LIB) -lc -lgcc

linhigh.bin: linhigh.elf
		$(ARMGNU)-objcopy linhigh.elf -O binary linhigh.bin

linldr.list: linhigh.elf
		$(ARMGNU)-objdump -D linhigh.elf > linldr.list

linhigh.obj: linhigh.bin
		$(ARMGNU)-objcopy -I binary -O elf32-littlearm -B arm linhigh.bin linhigh.obj
