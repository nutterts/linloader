#include <stdio.h>

#include "rpi.h"
#include "gpio.h"
#include "aux.h"

void
kmain(uint pc)
{
    muart.Init(115200);

    gpio.Mode(GPLED_ACKOK, GPMODE_OUT);
    while (1)
    {
        gpio.Set(GPLED_ACKOK);
        dummy(0x100000);
        gpio.Clear(GPLED_ACKOK);
        dummy(0x100000);
        iprintf("0x%08X\r\n", pc);
    }
}
