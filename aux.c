#include "rpi.h"
#include "gpio.h"
#include "aux.h"

/// MINI-UART

// Setup mini-uart @ baudrate
void aux_muinit(int baud)
{
//- Setup GPIO

    // Uart TX
    gpio.Mode(GPUART_TX, GPMODE_ALT5);
    gpio.AssertClk(GPUART_TX);

    // Uart RX
    gpio.Mode(GPUART_RX, GPMODE_ALT5);
    gpio.AssertClk(GPUART_RX);

//- Enable peripheral & set defaults
    poke(AUX_ENABLES, (peek(AUX_ENABLES) & ~1) | 1);
    poke(AUX_MU_IER_REG, 0);
    poke(AUX_MU_CNTL_REG, 0);
    poke(AUX_MU_LCR_REG, 0x3);
    poke(AUX_MU_MCR_REG, 0);
    poke(AUX_MU_IER_REG, 0);
    poke(AUX_MU_IIR_REG, 0xC6);

//- Set baudrate
    poke(AUX_MU_BAUD_REG, ((AUX_MU_CLOCK/baud)/8)-1);

//- Enable transciever
    poke(AUX_MU_CNTL_REG, 0x3);
}

// Mini-uart I/O
void aux_mupoke(char c)
{
    while (1)
    {
        if (peek(AUX_MU_LSR_REG) & 0x20) break;
    }
    poke(AUX_MU_IO_REG, c);
}

char aux_mupeek(void)
{
    while (1)
    {
        if (peek(AUX_MU_LSR_REG) & 1) break;
    }
    return peek(AUX_MU_IO_REG) & 0xFFFF;
}

/// SPI
/*
void aux_spi_init(int clk)
{
    unsigned int x;

    // Enable SPI0
    x=peek(AUX_ENABLES);
    x |= 2;
    poke(AUX_ENABLES, x);

    gpio.Mode(7, GPMODE_ALT0);
    gpio.Mode(8, GPMODE_ALT0);
    gpio.Mode(9, GPMODE_ALT0);
    gpio.Mode(10, GPMODE_ALT0);
    gpio.Mode(11, GPMODE_ALT0);

    poke(AUX_SPI0_CS, 0x0000030);
    poke(AUX_SPI0_CLK, 0x0000);
}
*/

/// OBJECTS

// Mini-uart
muart_obj muart = {
                   .My = {
                          ._magic = OBJ_MAGIC,
                          .Id = MUART_OBJ,
                          },
                   .Init = aux_muinit,
                   .Peek = aux_mupeek,
                   .Poke = aux_mupoke,
                   };

spi_obj spi = {
               .My = {
                      ._magic = OBJ_MAGIC,
                      .Id = SPI_OBJ,
                      },
               };
