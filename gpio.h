#ifndef GPIO_H
#define GPIO_H

#include "rpi.h"
#include "obj.h"

/// LED PINS
extern int GPLED_ACKOK;
extern int GPLED_POWER;

extern int GPSPI_CE0;
extern int GPSPI_CE1;
extern int GPSPI_MISO;
extern int GPSPI_MOSI;
extern int GPSPI_SCLK;

/// UART PINS
extern int GPUART_TX;
extern int GPUART_RX;

/// PIN PULL-UP/DOWN
typedef enum {
    GPPUD_OFF, GPPUD_DOWN, GPPUD_UP
} gppud_mode;

/// PIN MODE/STATE
typedef enum {
    GPMODE_IN, GPMODE_OUT, GPMODE_ALT5, GPMODE_ALT4, GPMODE_ALT0, GPMODE_ALT1, GOMODE_ALT2, GPMODE_ALT3
} gpmode;

/// FUNCTION POINTERS
typedef void (*gpio_modefunc)(int, gpmode);

/// OBJECT
typedef struct {
    obj My;
    gpio_modefunc Mode;
    voidint_func Set;
    voidint_func Clear;
    voidint_func AssertClk;
} gpio_obj;
extern gpio_obj gpio;

/// REGISTERS

// Memory offset
#define GP_BASE RPI_PBASE+0x200000

// Function Select
#define GPFSEL0 GP_BASE+0x0
#define GPFSEL1 GP_BASE+0x4
#define GPFSEL2 GP_BASE+0x8
#define GPFSEL3 GP_BASE+0xC
#define GPFSEL4 GP_BASE+0x10
#define GPFSEL5 GP_BASE+0x14

// Pin Output Set
#define GPSET0 GP_BASE+0x1C
#define GPSET1 GP_BASE+0x20

// Pin Output Clear
#define GPCLR0 GP_BASE+0x28
#define GPCLR1 GP_BASE+0x2C

// Pin Pull-up/down Enable
#define GPPUD GP_BASE+0x94

// Pin Pull-up/down Enable Clock
#define GPPUDCLK0 GP_BASE+0x98
#define GPPUDCLK1 GP_BASE+0x9C

#endif // GPIO_H
