.section ".text.boot"

.globl __entry
.extern kmain

__entry:
    mov sp, #0x5000000  // High stack
    bl kmain            // Copy loader
    mov r0, #0x5000000  // High entry
    bx r0               // Lift off

