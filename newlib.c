#include <_ansi.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <sys/times.h>
#include <errno.h>
#include <reent.h>

#include "linldr.h"
#include "aux.h"

#define heap_end 0x4EFFFFF

int heap_ptr = 0x4000000;
int prev_heap_ptr;

caddr_t
_sbrk (int incr)
{
    prev_heap_ptr = heap_ptr;
    heap_ptr += incr;
    if ((heap_ptr > 0x4EFFFFF) | (heap_ptr < 0x4000000))
    {
        // Out-of-bounds
        heap_ptr =  prev_heap_ptr; // Preserve original heap_ptr
        if (incr)
        {
            errno = ENOMEM; // Not enough memory availible.
        }
        else
        {
            errno = EFAULT; // Memory manage fault, more memory released then availible.
        }
        return (caddr_t) -1;
    }
    return (caddr_t) prev_heap_ptr;
}

/* Forward prototypes.  */
int     _system     _PARAMS ((const char *));
int     _rename     _PARAMS ((const char *, const char *));
int     isatty      _PARAMS ((int));
clock_t _times      _PARAMS ((struct tms *));
int     _gettimeofday   _PARAMS ((struct timeval *, struct timezone *));
void    _raise      _PARAMS ((void));
int     _unlink     _PARAMS ((void));
int     _link       _PARAMS ((void));
int     _stat       _PARAMS ((const char *, struct stat *));
int     _fstat      _PARAMS ((int, struct stat *));

int     _getpid     _PARAMS ((int));
int     _kill       _PARAMS ((int, int));
void    _exit       _PARAMS ((int));
int     _close      _PARAMS ((int));
int     _open       _PARAMS ((const char *, int, ...));
int     _write      _PARAMS ((int, char *, int));
int     _lseek      _PARAMS ((int, int, int));
int     _read       _PARAMS ((int, char *, int));
void    initialise_monitor_handles _PARAMS ((void));


//static int
//remap_handle (int fh)
//{
//return 0;
//}

void
initialise_monitor_handles (void)
{
}

//static int
//get_errno (void)
//{
//return(0);
//}

//static int
//error (int result)
//{
//errno = get_errno ();
//return result;
//}


int
_read (int file,
       char * ptr,
       int len)
{
    return len;
}


int
_lseek (int file,
        int ptr,
        int dir)
{
    return 0;
}


int
_write (int    file,
        char * ptr,
        int    len)
{
    int r;
    for(r=0; r<len; r++) muart.Poke(ptr[r]);
    return len;
}

int
_open (const char * path,
       int          flags,
       ...)
{
    return 0;
}


int
_close (int file)
{
    return 0;
}

void
_exit (int n)
{
    while(1);
}

int
_kill (int n, int m)
{
    return(0);
}

int
_getpid (int n)
{
    return 1;
    n = n;
}




int
_fstat (int file, struct stat * st)
{
    return 0;
}

int _stat (const char *fname, struct stat *st)
{
    return 0;
}

int
_link (void)
{
    return -1;
}

int
_unlink (void)
{
    return -1;
}

void
_raise (void)
{
    return;
}

int
_gettimeofday (struct timeval * tp, struct timezone * tzp)
{
    if(tp)
    {
        tp->tv_sec = 10;
        tp->tv_usec = 0;
    }
    if (tzp)
    {
        tzp->tz_minuteswest = 0;
        tzp->tz_dsttime = 0;
    }
    return 0;
}

clock_t
_times (struct tms * tp)
{
    clock_t timeval;

    timeval = 10;
    if (tp)
    {
        tp->tms_utime  = timeval;   /* user time */
        tp->tms_stime  = 0; /* system time */
        tp->tms_cutime = 0; /* user time, children */
        tp->tms_cstime = 0; /* system time, children */
    }
    return timeval;
};


int
_isatty (int fd)
{
    return 1;
    fd = fd;
}

int
_system (const char *s)
{
    if (s == NULL)
        return 0;
    errno = ENOSYS;
    return -1;
}

int
_rename (const char * oldpath, const char * newpath)
{
    errno = ENOSYS;
    return -1;
}
